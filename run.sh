#!/bin/bash
main() {
    # get required env variables
    init
    if [ ${COMMAND} == "deploy" ];then
      deploy_app
    else 
      # run command
      run_command
    fi
}

function init() {
    export AWS_ACCESS_KEY_ID=$AWS_ACCESS_KEY_ID
    export AWS_SECRET_ACCESS_KEY=$AWS_SECRET_ACCESS_KEY
    export AWS_DEFAULT_REGION=$AWS_DEFAULT_REGION
}

function run_command() {
    cd infra/
    # initialize your Terraform workspace
    terraform init
    # run command
    if [ ${COMMAND} == "apply" ];then
      terraform ${COMMAND} -auto-approve
    elif [ ${COMMAND} == "destroy" ];then
      terraform ${COMMAND} -force
    else
      terraform ${COMMAND}
    fi
}

function deploy_app() {
    aws eks --region $AWS_DEFAULT_REGION update-kubeconfig --name eks-cluster-with-terraform
    cd deploy/
    kubectl apply -f hello-kubernetes.yml
    sleep 10
    # validation
    for i in {1..100}; do sleep 1; if [ `kubectl get deploy -o jsonpath='{.items[*].status.readyReplicas}'` == "3" ] 2>/dev/null; then echo "The app is running" && exit 0;else echo "Deployment is not ready..."; fi; done; echo "The app is not healthy" && exit 1 
}

main "$@"