# AWS EKS with Terraform 

## **Description**

Provision of EKS clusters with Terraform and GitLab.

## **Prerequisites**

1- Create an S3 bucket called **terraform-state-file-09-10-20** for remote state file.

2- Add your **AWS_ACCESS_KEY_ID**, **AWS_SECRET_ACCESS_KEY** and **AWS_DEFAULT_REGION** keys to GitLab CI / CD variables.

Settings -> CI / CD -> Variables

## **Configuration**

There are 4 stages plan, apply, deploy and destroy.

```yaml
plan-infra:
  stage: plan
  when: manual
  script:
  -  chmod +x run.sh && COMMAND="plan" ./run.sh
  except:
  - schedules 

apply-infra:
  stage: apply
  when: manual
  script:
  - chmod +x run.sh && COMMAND="apply" ./run.sh
  except:
  - schedules
```

Provisions a VPC, subnets and availability zones using the AWS VPC Module.
```
https://registry.terraform.io/modules/terraform-aws-modules/vpc/aws/2.32.0
```

Provisions all the resources required to set up an EKS cluster using the AWS EKS Module.
```
https://registry.terraform.io/modules/terraform-aws-modules/eks/aws/11.0.0
```

```
module "eks" {
  source       = "terraform-aws-modules/eks/aws"
  cluster_name = "${var.cluster_name}"
  cluster_version = "1.17"
  subnets      = module.vpc.private_subnets

  vpc_id = module.vpc.vpc_id

  worker_groups = [
    {
      name                 = "worker-group"
      instance_type        = "t2.small"
      asg_desired_capacity = 3
    }
  ]
}
```

Run the following command to automatically configure kubectl.
```
aws eks --region $AWS_DEFAULT_REGION update-kubeconfig --name eks-cluster-with-terraform
```

Run deployment file using **paulbouwer/hello-kubernetes:1.8** image.
```yaml
spec:
  containers:
  - name: hello-kubernetes
    image: paulbouwer/hello-kubernetes:1.8
```

Run service file with LoadBalancer type.
```yaml
apiVersion: v1
kind: Service
metadata:
  name: hello-kubernetes
spec:
  type: LoadBalancer
```

## **Validation**

Validate if your application is up and running in the pipeline using JSONPath.

```
kubectl get deploy -o jsonpath='{.items[*].status.readyReplicas}
```
Job succeeded
```
service/hello-kubernetes created
deployment.apps/hello-kubernetes created
Deployment is not ready...
Deployment is not ready...
Deployment is not ready...
Deployment is not ready...
The app is running
```
Job failed
```
Deployment is not ready...
Deployment is not ready...
Deployment is not ready...
Deployment is not ready...
Deployment is not ready...
Deployment is not ready...
The app is not healthy
Cleaning up file based variables
ERROR: Job failed: exit code 1
```

Access your application using DNS name of your load balancer on port 80.
```
curl -silent *****.eu-east-1.elb.amazonaws.com:80 | grep title
```

## **Destroy**

Destroys all infrastructure resources.
```yaml
destroy-infra:
  stage: destroy
  when: manual
  script:
  - chmod +x run.sh && COMMAND="destroy" ./run.sh
  except:
  - schedules
```